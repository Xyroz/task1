package main 

//import "fmt"
import "net/http"
import "os"
import "strings"
import "encoding/json"

//BASEURL of where to send GET requests
const BASEURL 	string = "https://api.github.com/repos/"
//CONTRURL to be added to get contributors
const CONTRURL 	string = "/contributors"
//LANGURL to be added to get languages
const LANGURL 	string = "/languages"

//Project struct to be decoded into from /repos/...
type Project struct {										 
	Name 		string 	`json:"full_name"`					
	Owner 		struct {
		Name 	string 	`json:"login"`						
	} 					`json:"owner"`
}
		
//Contributor struct to decoded into from /contributors
type Contributor struct {
	Name 			string 	`json:"login"`
	Contributions 	int 	`json:"contributions"`
}

//ReturnStruct to encode the response.
type ReturnStruct struct {
	Name 		string 	`json:"repository"`
	Owner 		string 	`json:"owner"`
	Committer 	string 	`json:"comitter"`
	Commits 	int 	`json:"commits"`
	Languages []string 	`json:"language"`
}

//handler Handles all requests from /projectinfo/v1/
func handler(w http.ResponseWriter, r *http.Request){

	project := Project{}
	contributors := []Contributor{}
	answer := ReturnStruct{}
	var languages map[string]int

	url := splitMakeURL(r.URL.Path)

	if url == "" {
		status := 400
		http.Error(w, http.StatusText(status), status)
		return
	}

	err := getRepo(url, &project)

	if err != nil {
		status := 503
		http.Error(w, http.StatusText(status), status)
		return 
	}

	err = getComm(url, &contributors)

	if err != nil {
		status := 503
		http.Error(w, http.StatusText(status), status)
		return 
	}

	err = getLang(url, &languages)

	if err != nil {
		status := 503
		http.Error(w, http.StatusText(status), status)
		return 
	}

	createJSON(project, contributors, languages, &answer)

	json.NewEncoder(w).Encode(&answer)
}

//splitMakeUrl takes an url as paramter and splits it based on the "\", 
//returns constructed url or blank if url isn't long enough, or last part is empty
func splitMakeURL(s string) string {

	parts := strings.Split(s, "/")
	if len(parts) != 6 || parts[5] == ""{
		return ""
	}

	r := BASEURL + parts[4] + "/" + parts[5]
	return r
}

//getRepo gets json from the project url, decodes into project struct
//returns nil if sucsessful, else return error
func getRepo(u string, p *Project) error {

	repoInf, err := http.Get(u)

	if err == nil {
		json.NewDecoder(repoInf.Body).Decode(&p)
	}

	return err
}

//getComm gets json from the contributor url, decodes into array of contributor structs
//returns nil if sucsessful, else return error
func getComm(u string, c *[]Contributor) error {

	commInf, err := http.Get(u + CONTRURL)

	if err == nil {
		json.NewDecoder(commInf.Body).Decode(&c)
	}

	return err
}

//getLang gets json from the language url, decodes into a map 
//returns nil if sucsessfull, else return error
func getLang(u string, l *map[string]int) error {

	langInf, err := http.Get(u + LANGURL)
	
	if err == nil {
		json.NewDecoder(langInf.Body).Decode(&l)
	}

	return err
}

//createJSON ready's result into a ReturnStruct
func createJSON(p Project, c []Contributor, l map[string]int, a *ReturnStruct) {

	a.Name = p.Name
	a.Owner = p.Owner.Name
	a.Committer = c[0].Name
	a.Commits = c[0].Contributions

	for k := range l {
		a.Languages = append(a.Languages, k)
	}
}

func main() {
	http.HandleFunc("/projectinfo/v1/", handler)
	http.ListenAndServe(":" + os.Getenv("PORT"), nil)
}